# -- Software Stack Version

SPARK_VERSION="2.4.8"
HADOOP_VERSION="2.7"

# -- Building the Images
set -e

docker build -t cluster-base:latest ./docker/cluster-base

docker build \
  --build-arg spark_version="${SPARK_VERSION}" \
  --build-arg hadoop_version="${HADOOP_VERSION}" \
  -t spark-base:latest ./docker/spark-base

docker build -t spark-master:latest ./docker/spark-master
docker build -t spark-worker:latest ./docker/spark-worker
docker build -t twitter-server:latest ./docker/twitter-server
