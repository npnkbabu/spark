# Spark Cluster with Docker & docker-compose

# General

A simple spark standalone cluster for your testing environment purposses. A *docker-compose up* away from you solution for your spark development environment.

The Docker compose will create the following containers:

container|Ip address:port
      ---|---
spark-master|172.18.0.2:8080
spark-worker-1|172.18.0.3:8081

# Installation

The following steps will make you run your spark cluster's containers.

## Pre requisites

* Docker installed

* Docker compose  installed
we need to create a custom network with below command 
docker network create -d bridge --subnet 192.168.0.0/24 --gateway 192.168.0.1 dockernet
and then docker-compose up


## Build the images
https://gist.github.com/dkurzaj/2a899de8cb5ae698919f0a9bbf7685f0


The first step to deploy the cluster will be the build of the custom images, these builds can be performed with the *build.sh* script. 

The executions is as simple as the following steps:

```sh
chmod +x images.sh
./images.sh
```

This will create the following docker images:

* cluster-base : its the base machine image in cluster with jdk8
* spark-base : its the spark machine image on top of cluster-base with spark 3.1.1 and hadoop 2.7
* spark-master : its the master image built on top of spark-base
* spark-worker : its worker image built on top of spark-base

## kafka
https://github.com/wurstmeister/kafka-docker
setting kafka container
set kafka from docker-compose and mention topic "KAFKA_CREATE_TOPICS". in order to test it use below procedure
a. set 2 bash terminals from  kakfa container. one for producer and another for consumer
b. enter below command in producer to send a message to created topic (provided you already created "test" topic from KAFKA_CREATE_TOPICS). It will show ">" waiting for a message
kafka-console-producer.sh --broker-list localhost:9092 --topic test
c. come to consumer terminal and enter below command to start listening 
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test
d. come back to producer terminal and enter a message. which should appear in consumer terminal

## Run the docker-compose

The final step to create your test cluster will be to run the compose file:

```sh
docker-compose up
```

## Validate your cluster

Just validate your cluster accesing the spark UI on each worker & master URL.

# UIs
### Spark Master
http://192.168.0.3:8080/


### kafka UI
http://192.168.0.6:9000/

# kafka cluster creation in CMAK (CMAK server : kafka manager server)
1. go to kafka UI http://192.168.0.5:9000/
2. Add cluster (https://www.youtube.com/watch?v=enqqp2ZIEyE)
3. Sometimes after adding the cluster you may not see the default topic and broker creation.
so in this case just restart docker
4. create topic (retention.ms in updateconfig = 60000  means 1 min it will keep the data)
5. when creating a cluster , you choose kafka version, this version should be comaptable with the spark version what we built in dockers
6. install pyspark 2.4.8 on local machine.

# Resource Allocation 

This cluster is shipped with 1 worker and one spark master, each of these has a particular set of resource allocation(basically RAM & cpu cores allocation).

* The default CPU cores allocation for each spark worker is 1 core.

* The default RAM for each spark-worker is 1 GB.

* The default RAM allocation for spark executors is 512 MB.

* The default RAM allocation for spark driver is 256 MB

* If you wish to modify this allocations just edit the docker-compose file.

# Binded Volumes

To make app running easier I've shipped 1 volume mount described in the following chart:

Host Mount|Container Mount|Purposse
        ---|---|---
shared-workspace|/opt/workspace|Used to make available your app's data on all workers & master


# Summary (What have I done :O?)

* We compiled the necessary docker images to run spark master and worker containers.

* We created a spark standalone cluster using 1 worker nodes and 1 master node using docker && docker-compose.

* Copied the resources necessary to run a sample application.

* Submitted an application to the cluster using a **spark-submit** docker image.

* We ran a distributed application at home(just need enough cpu cores and RAM to do so).

# Why a standalone cluster?

* This is intended to be used for test purposses, basically a way of running distributed spark apps on your laptop or desktop.

* Right now I don't have enough resources to make a Yarn, Mesos or Kubernetes based cluster :(.

* This will be useful to use CI/CD pipelines for your spark apps(A really difficult and hot topic)

# sample tweet object 
{
 "created_at": "Wed Oct 10 20:19:24 +0000 2018",
 "id": 1050118621198921728,
 "id_str": "1050118621198921728",
 "text": "To make room for more expression, we will now count all emojis as equal—including those with gender‍‍‍ ‍‍and skin t… https://t.co/MkGjXf9aXm",
 "user": {},  
 "entities": {}
}