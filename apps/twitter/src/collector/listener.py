
import json
from kafka import KafkaProducer
import tweepy
from configuration import TwitterConfiguration

class TwitterListener(tweepy.StreamListener):
    """ A class to read the twitter stream and push it to Kafka"""

    def __init__(self, api):
        self.__config = TwitterConfiguration.getConfig()
        self.api = api
        super(tweepy.StreamListener, self).__init__()
        self.producer = KafkaProducer(bootstrap_servers=self.__config['kafka']['server:port'],
                                     value_serializer=self.json_serializer)
        
    def json_serializer(self,data):
        return json.dumps(data).encode('utf-8')
    
    def on_status(self, msg):
        """ This method is called whenever new data arrives from live stream.
        We asynchronously push this data to kafka queue"""
        try:
            self.producer.send('twitterstream', msg.text)
            print(msg.text)
        except Exception as e:
            print(e)
            return False
        return True

    def on_error(self, status_code):
        print("Error received in kafka producer")
        return True # Don't kill the stream

    def on_timeout(self):
        return True # Don't kill the stream
