
import tweepy
from listener import TwitterListener
from configuration import TwitterConfiguration

class TwitterDataCollector:
    def __init__(self):
        self.__config = TwitterConfiguration.getConfig()
    
    def Process(self,hashtags):
        consumer_key = self.__config['twitter']['api']['consumerKey']
        consumer_secret = self.__config['twitter']['api']['consumerSecret']
        access_key = self.__config['twitter']['api']['accessKey']
        access_secret = self.__config['twitter']['api']['accessSecret']

        # Create Auth object
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_key, access_secret)
        api = tweepy.API(auth)

        # Create stream and bind the listener to it
        stream = tweepy.Stream(auth, listener = TwitterListener(api))

        #Custom Filter rules pull all traffic for those filters in real time.
        stream.filter(track = hashtags, languages = ['en'])
        #stream.filter(locations=[-180,-90,180,90], languages = ['en'])

