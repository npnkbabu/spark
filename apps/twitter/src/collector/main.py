import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
import sys
sys.path.append(os.path.join(BASE_DIR,'src/utils'))

from dataCollector import TwitterDataCollector

if __name__ == '__main__':
    obj = TwitterDataCollector()
    obj.Process(['COVID'])