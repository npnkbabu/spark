
from __future__ import print_function
from pyspark.sql import SparkSession
from configuration import TwitterConfiguration
from pyspark.sql.types import StringType
from pyspark.sql.functions import from_json
from pyspark.sql.types import StructType, StructField, BooleanType, LongType, IntegerType
from pyspark.sql.functions import col, from_unixtime, to_date, to_timestamp

class TwitterDataAnalyzer():
    '''
    This class is to extract data from kafka and do analysis
    '''
    def __init__(self):
        self.__config = TwitterConfiguration.getConfig()
        spark = SparkSession.builder.appName('Twitter trend analysis')\
                                    .master(self.__config['spark']['master'])\
                                    .config('spark.jars.packages',self.__config['spark']['kafkaPkg'])\
                                    .getOrCreate()
        
        dfStream = spark \
            .readStream \
            .format('kafka') \
            .option('kafka.bootstrap.servers', self.__config['kafka']['server:port']) \
            .option('subscribe', self.__config['kafka']['topicname']) \
            .option('startingOffsets', 'earliest') \
            .load()

        # Convert binary to string key and value
        dfStream_string = (dfStream
            .withColumn('key', dfStream['key'].cast(StringType()))
            .withColumn('value', dfStream['value'].cast(StringType())))

        schema_twitter = StructType(
            [StructField("text",StringType(),True)]
        )
        self.df_tweets = (dfStream_string.withColumn('value',from_json('value',schema_twitter)))

        print('hi')
    def Process(self):
        lines = self.dfStream.map(lambda x: x[1].encode('ascii', 'ignore'))
        #lines=[]
        #window interval is 60, so it will calculate for 60sec of data. 
        # means it will go 60sec back and gets data. sliding interval means 
        # when my compuation happens, since we did not mention it , it will be same as batch interval
        hashtags = lines.flatMap(lambda text : text.split(' ')).\
                        filter(lambda word : word.lower().startswith('#')).\
                        map(lambda word : (word.lower(),1)).reduceByKey(lambda a,b: a+b)

        author_counts_sorted_dstreams = hashtags.transform(lambda foo: foo.sortBy(lambda x : x[0].lower()).\
                                                                           sortBy(lambda x : x[1],ascending=False))
        author_counts_sorted_dstreams.pprint()
        self.__sparkStreamingContext.start()
        self.__sparkStreamingContext.awaitTermination()




