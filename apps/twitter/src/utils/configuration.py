import os
import json

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
CONFIG_PATH = os.path.join(BASE_DIR,'config')
DATA_PATH = os.path.join(BASE_DIR,'data')
CONFIG_FILE = 'twitter.json'

#load configuration

class TwitterConfiguration:

    @staticmethod
    def getConfig():
        with open(os.path.join(os.path.join(CONFIG_PATH,CONFIG_FILE)), 'r') as file:
            config = json.load(file)
            return config
    